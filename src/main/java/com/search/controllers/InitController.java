package com.search.controllers;

import com.search.engine.collector.QueryHandler;
import com.search.engine.util.Transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

import static java.util.Objects.nonNull;
import static org.apache.commons.validator.routines.UrlValidator.getInstance;

@Controller
@RequestMapping("/")
public class InitController {

    @Autowired
    private QueryHandler queryHandler;

    @RequestMapping(value = "/")
    public ModelAndView init() {
        return new ModelAndView("search");
    }

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String indexing(@RequestParam(value = "q", required = false) String url,
                           @RequestParam(value = "depth", required = false) Integer depth, ModelMap map) {
        if (url != null && getInstance().isValid(url)) {
            if (depth != null && depth > 0) {
                map.addAttribute("url", url);
                queryHandler.index(url, depth);
            }else {
                map.addAttribute("url", url);
                queryHandler.index(url);
            }
            return "indexing";
        } else
            return "index";
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String fullTextSearch(@RequestParam(value = "q") String query, ModelMap map) throws InterruptedException {
        Map<String, String> m = queryHandler.search(query);
        if (nonNull(m)) {
            map.put("size", m.size());
            map.put("results", Transfer.transformResult(m).entrySet());
        } else {
            map.put("size", 0);
        }
        return "results";
    }
}