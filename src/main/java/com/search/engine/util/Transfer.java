package com.search.engine.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Transfer {
    public static Map<String, String> transformResult(Map<String, String> docAttribute) {
        Map<String, String> res = new ConcurrentHashMap<>(docAttribute.size());
        for (Map.Entry<String, String> entry : docAttribute.entrySet()) {
            res.put(ensureValidJspx(entry.getKey()),
                    ensureValidJspx(entry.getValue()));
        }
        return res;
    }

    private static String ensureValidJspx(String content) {
        return content.replaceAll("<", "&lt;").replaceAll("&", "&amp;").replaceAll("\"", "&quot;").replaceAll(">", "&gt;");
    }
}