package com.search.engine;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.lucene.index.IndexWriterConfig.OpenMode;

public class FullTextSearch {

    private static FullTextSearch instance = null;

    private final String indexPath = "index";

    private IndexWriter writer;
    private Analyzer    analyzer;

    protected FullTextSearch() {
    }

    public void openIndex() {
        try {
            Directory dir = FSDirectory.open(new File(indexPath));
            analyzer = new StandardAnalyzer();
            IndexWriterConfig config = new IndexWriterConfig(Version.LATEST, analyzer);
            config.setOpenMode(OpenMode.CREATE_OR_APPEND);
            writer = new IndexWriter(dir, config);
        } catch (Exception ignored) {
        }
    }

    public void indexDoc(String url, String title, String content) throws IOException {
        Document document = new Document();
        StringField urlField = new StringField("url", url, Field.Store.YES);
        document.add(urlField);
        StringField titleField = new StringField("title", title, Field.Store.YES);
        document.add(titleField);
        TextField contentField = new TextField("content", content, Field.Store.NO);
        document.add(contentField);
        synchronized (this) {
            if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
                writer.addDocument(document);
            } else {
                writer.updateDocument(new Term("content", content), document);
            }
            writer.commit();
        }
    }

    public Map<String, String> search(String query) {
        Map<String, String> result = null;
        try {
            QueryParser parser = new QueryParser("content", analyzer);
            Query q = parser.parse(query);
            int hitsPerPage = 10;
            Directory fsDir = FSDirectory.open(new File(indexPath));
            DirectoryReader reader = DirectoryReader.open(fsDir);
            IndexSearcher searcher = new IndexSearcher(reader);
            TopScoreDocCollector collector = TopScoreDocCollector.create(
                    hitsPerPage, true);
            searcher.search(q, collector);
            ScoreDoc[] hits = collector.topDocs().scoreDocs;
            result = new HashMap<>(hits.length);
            for (ScoreDoc hit : hits) {
                int docId = hit.doc;
                Document d = searcher.doc(docId);
                result.put(d.get("url"), d.get("title"));
            }
        } catch (Exception e) {
            System.out.println("Got an Exception: " + e.getMessage());
        }
        return result;
    }

    public static FullTextSearch getInstance() {
        if (instance == null) {
            instance = new FullTextSearch();
            instance.openIndex();
        }
        return instance;
    }
}