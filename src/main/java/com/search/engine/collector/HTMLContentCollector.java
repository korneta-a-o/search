package com.search.engine.collector;

import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import static java.util.Objects.requireNonNull;

public class HTMLContentCollector {

    private static HTMLContentCollector instance = null;
    private String      url;
    private int         depthPassage;
    private Set<String> urls;
    private final UrlValidator urlValidator = UrlValidator.getInstance();

    private HTMLContentCollector() {
    }

    public Set<String> nestedUrls() {
        urls = new ConcurrentSkipListSet<>();
        try {
            nestedUrls(requireNonNull(url), depthPassage);
        } catch (IOException ignored) {
        }
        urls.add(url);
        return urls;
    }

    private void nestedUrls(String link, int deep) throws IOException {
        String s;
        if (deep > 1) {
            --deep;
            for (Element element : Jsoup.connect(link).get().getElementsByTag("a")) {
                s = element.attr("href");
                if (urlValidator.isValid(s) && !urls.contains(s)) {
                    urls.add(s);
                    nestedUrls(s, deep);
                }
            }
        }
    }

    public String extractPlainText(String docRef) throws IOException {
        return Jsoup.parse(Jsoup.connect(docRef).get().html()).text();
    }

    public String extractTitle(String docRef) throws IOException {
        return Jsoup.connect(docRef).get().title();
    }

    public Set<String> getUrls() {
        return urls;
    }

    public int getDepthPassage() {
        return depthPassage;
    }

    public UrlValidator getUrlValidator() {
        return urlValidator;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDepthPassage(int depthPassage) {
        this.depthPassage = depthPassage;
    }

    public void setUrls(Set<String> urls) {
        this.urls = urls;
    }

    public static HTMLContentCollector getInstance() {
        if (instance == null) {
            instance = new HTMLContentCollector();
        }
        return instance;
    }
}
