package com.search.engine.collector;

import com.search.engine.FullTextSearch;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

import static java.util.Objects.requireNonNull;

@Service
public class QueryHandler {

    public static final int                  DEFAULT_DEPTH = 5;
    public static final FullTextSearch       ENGINE        = FullTextSearch.getInstance();
    public static final HTMLContentCollector COLLECTOR     = HTMLContentCollector.getInstance();

    public void index(String url) {
        index(url, DEFAULT_DEPTH);
    }

    public void index(String url, int depth) {
        COLLECTOR.setUrl(url);
        COLLECTOR.setDepthPassage(depth);
        for (String s : COLLECTOR.nestedUrls()) {
            try {
                ENGINE.indexDoc(s, COLLECTOR.extractTitle(s), COLLECTOR.extractPlainText(s));
            } catch (IOException ignored) {
            }
        }
    }

    public Map<String, String> search(String query) {
        return ENGINE.search(requireNonNull(query));
    }

}
