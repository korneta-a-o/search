/**
 * Created by Antoha on 09.02.2015.
 */
$(document).ready(function () {
    inputBehavior();
    onKeyPress();
    searchQuery();
});

function inputBehavior() {
    var a = $("#search");
    a.focus(function () {
        $("#centred-holder").css({'border': '1px solid rgba(53, 119, 216, 1)'});
    });

    a.focusout(function () {
        rid();
    });

    a.hover(function () {
        $("#centred-holder").css({'border': '1px solid rgba(168, 168, 168, 1)'});
    }, function () {
        if (!$("#index").is(":focus")) {
            rid();
        }
    });
}

function rid() {
    $("#centred-holder").css({'border': '1px solid rgba(0, 0, 0, 0.15)'});
}

function onKeyPress() {
    $("#search").keyup(function (event) {
        if (event.keyCode == 13) {
            redirectToRes();
        }
        if (!($(this).val() == '')) {
            $("#autocomplete").css({'display': 'block'});
        } else {
            $("#autocomplete").css({'display': 'none'});
        }
    });
}

function redirectToRes() {
    var query = $("#search").val();
    if (!(query == '')) {
        document.location.href = '/search?q=' + query;
    }
}
function searchQuery() {
    $("#search-button").click(function () {
        redirectToRes();
    });
}